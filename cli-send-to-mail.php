<?php
$from = "feedback@one-print.ru";
$to = "9916713@mail.ru";
//$to = "net-services@yandex.ru";

$subject = "Новый заказ шаблона с сайта one-print.ru";

$message = "";

switch ($_GET["text-type"]) {
    case "text-type-1":
        $message .= "Тип вывески: Световые объемные буквы\n";
        break;
    case "text-type-2":
        $message .= "Тип вывески: Объемные буквы со световым торцом и лицом\n";
        break;
    case "text-type-3":
        $message .= "Тип вывески: Объемные буквы с контражурной подсветкой\n";
        break;
    case "text-type-4":
        $message .= "Тип вывески: Объемные буквы без подсветки\n";
        break;
    case "text-type-5":
        $message .= "Тип вывески: Плоские буквы\n";
        break;
    case "text-type-6":
        $message .= "Тип вывески: Плоские буквы с контражурной подсветкой\n";
        break;
    default:
        return 0;
        break;
}

$message .= "Надпись " . $_GET["text-preview"] . "\n";
$message .= "Выбранный шрифт " . $_GET["font-family"] . "\n";
$message .= "Высота вывески: " . $_GET["text-height"] . "\n";
$message .= "Длина вывески: " . $_GET["text-width"] . "\n";
$message .= "Цвет лицевой части: " . $_GET["text-face"] . "\n";

switch ($_GET["text-type"]) {
    case "text-type-1":
    case "text-type-2":
    case "text-type-3":
    case "text-type-4":
        $message .= "Цвет борта или подсветки: " . $_GET['text-board'] . "\n";
    default:
        break;
}

if ($_GET['substrate'] == 'yes') {
    $message .= "Подложка" . "\n";
    $message .= "Цвет подложки: " . $_GET['substrate-color'] . "\n";
    $message .= "Высота подложки: " . $_GET['substrate-size-h'] . "\n";
    $message .= "Длина подложки: " . $_GET['substrate-size-w'] . "\n";
} else {
    $message .= "Без подложки" . "\n";
}

// Цена
$message .=  "Цена за подложку: " . $_GET['price-board'] . "\n";
$message .=  "Цена за вывеску: " . $_GET['price-substrate'] . "\n";
$message .=  "Итоговая цена: " . $_GET['price'] . "\n";


// Контактная информация
$message .=  "Имя: " . $_GET['user-name'] . "\n";

$message .=  "Телефон: " . $_GET['user-tel'] . "\n";
$message .=  "Электронная почта: " . $_GET['user-email'] . "\n";
$message .=  "Комментарий: " . $_GET['user-comment'] . "\n";


$head = "MIME-Version: 1.0\r\n";
$head .= "Content-type: text/html; charset=utf-8\r\n";
$head .= "From: " . $from . "\r\n";
$head .= "Reply-To: " . $from . "E\r\n";
$res = [];
$res['message'] = $message;
if (mail($to, $subject, $message, $head)) {
    $res['status'] = 1;
    echo json_encode($res);
} else {
    $res['status'] = 0;
    echo json_encode($res);
}
