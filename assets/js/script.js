$(document).ready(function () {
  //
  var elTextPreview = ".text-preview"; // Вывеска
  //
  // Световые объемные буквы
  var elTextPreviewType1 = ".text-preview.text-type-1";
  var color1Text = "#fe9001";
  var color1TextShadow = "#fe9001 0px 0px 10px";
  var color1Board =
    "rgb(204, 204, 204) 0px 1px 0px, rgb(201, 201, 201) 0px 2px 0px, rgb(187, 187, 187) 0px 3px 0px, rgb(185, 185, 185) 0px 4px 0px, rgb(170, 170, 170) 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px, rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, rgb(0, 0, 0) 0px 6px 5px";

  //
  // Объемные буквы со световым торцом и лицом
  var elTextPreviewType2 = ".text-preview.text-type-2";
  var color2Text = "#fe9001";
  var color2TextShadow = "2px 0px 2px #ffffff, 0px 2px 2px #ffffff, -2px 0px 2px #ffffff, 0px -2px 2px #ffffff";
  var color2Board =
    "rgb(204, 204, 204) 0px 1px 0px, rgb(201, 201, 201) 0px 2px 0px, rgb(187, 187, 187) 0px 3px 0px, rgb(185, 185, 185) 0px 4px 0px, rgb(170, 170, 170) 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px, rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, 0px 6px 5px rgba(0, 0, 0, 1)";
  //
  //
  var elTextPreviewType3 = ".text-preview.text-type-3";
  var color3Text = "#fe9001";
  var color3Board =
    "rgb(204, 204, 204) 0px 1px 0px, rgb(201, 201, 201) 0px 2px 0px, rgb(187, 187, 187) 0px 3px 0px, rgb(185, 185, 185) 0px 4px 0px, rgb(170, 170, 170) 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px, rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, 10px 0px 15px #ffffff, 0px 10px 15px #ffffff, -10px 0px 15px #ffffff, 0px -10px 15px #ffffff, 0px 6px 5px rgba(0, 0, 0, 1)";

  //
  var elTextPreviewType4 = ".text-preview.text-type-4";
  var color4Text = "#fe9001";
  var color4Board =
    "rgb(204, 204, 204) 0px 1px 0px, rgb(201, 201, 201) 0px 2px 0px, rgb(187, 187, 187) 0px 3px 0px, rgb(185, 185, 185) 0px 4px 0px, rgb(170, 170, 170) 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px,rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, 0px 6px 5px rgba(0, 0, 0, 1)";

  var elTextPreviewType5 = ".text-preview.text-type-5";
  var elTextPreviewType6 = ".text-preview.text-type-6";
  var elSubstrateMathH = "#SubstrateMathH .value";
  var elSubstrateMathW = "#SubstrateMathW .value";
  var elSubstrate = "#Substrate"; // Подложка

  var elSubstrateOptions = "#FormGroupSubstrateOptions"; // Группа полей опций в форме
  var inputTextPreview = $("input[name=text-preview]"); // Текст вывески
  var inputSubstrateSizeH = $("input[name=substrate-size-h]");
  var inputSubstrateSizeW = $("input[name=substrate-size-w]");
  var inputTextFace = 'input[name="text-face"]';
  var inputTextBoard = 'input[name="text-board"]';

  var input = $("input#input-fon-load");

  $(".from-cli").trigger("reset");

  $("#SetFonHeight").change(function (e) {
    var value = $(this).val();
    $(".cll-preview--fon img").css("height", value + "%");
  });

  /**
   * Слайдер положения фонового рисунка
   */
  var SliderMoveFon;
  SliderMoveFon = $("#SliderMoveFon").slider({
    range: "min",
    value: 0,
    min: -100,
    max: 100,
    slide: function (event, ui) {
      console.log(ui.value);
      $(".cll-preview--fon img").css("transform", "translateX(" + ui.value + "%)");
    },
  });

  /**
   * Слайдер высоты букв
   */
  var sliderTextHeight;
  sliderTextHeight = $("#SliderHeight").slider({
    range: "min",
    value: 50,
    min: 10,
    max: 125,
    create: function () {
      let value = $(this).slider("value");
      $("#SliderHeightHandle").text(value + " см.");
      $("input[name=text-height]").val(value);
      calcPrice();
    },
    slide: function (event, ui) {
      updateBorder(ui.value);
      $("#SliderHeightHandle").text(ui.value + " см.");
      $("input[name=text-height]").val(ui.value);
      calcPrice();
    },
  });

  /**
   * Тип объемных букв
   * Выпадающие список
   */
  $("select[name=text-type]").change(function (e) {
    resetText();

    $(elTextPreview).addClass("hidden");
    $("#GroupColorTextBoard").addClass("disabled");

    switch ($(this).val()) {
      case "text-type-1":
        // Световые объемные буквы
        $(elTextPreviewType1).removeClass("hidden");
        $("#GroupColorTextBoard").removeClass("disabled");
        break;
      case "text-type-2":
        // Объемные буквы со световым торцом и лицом
        $(elTextPreviewType2).removeClass("hidden");
        $("#GroupColorTextBoard").removeClass("disabled");
        break;
      case "text-type-3":
        // Объемные буквы с контражурной подсветкой
        $(elTextPreviewType3).removeClass("hidden");
        $("#GroupColorTextBoard").removeClass("disabled");
        break;
      case "text-type-4":
        // Объемные буквы без подсветки
        $(elTextPreviewType4).removeClass("hidden");
        $("#GroupColorTextBoard").removeClass("disabled");
        break;
      case "text-type-5":
        // Плоские буквы
        $(elTextPreviewType5).removeClass("hidden");
        break;
      case "text-type-6":
        // Плоские буквы с контражурной подсветкой
        $(elTextPreviewType6).removeClass("hidden");
        break;
      default:
        break;
    }
  });

  $(".from-cli-question").click(function (e) {
    changeQuestionData();
    $(".from-cli-question-data").toggleClass("hidden");
  });

  /**
   * Выбор фона для вывески
   */
  $(".cll-preview-btns .btn-fon").click(function (e) {
    $(".cll-preview--fon").addClass("hidden");
    let target = $(this).attr("data-fon-img");
    $("#" + target).removeClass("hidden");

    switch (target) {
      case "cll-preview--fon-1":
        $(".cll-preview-text").css("left", "155px");
        $(".cll-preview-text").css("top", "50px");
        break;
      case "cll-preview--fon-2":
        $(".cll-preview-text").css("left", "505px");
        $(".cll-preview-text").css("top", "50px");
        //
        break;
      case "cll-preview--fon-3":
        $(".cll-preview-text").css("left", "160px");
        $(".cll-preview-text").css("top", "10px");
        break;
      case "cll-preview--fon-4":
        $(".cll-preview-text").css("left", "150px");
        $(".cll-preview-text").css("top", "0px");
        //
        break;

      default:
        break;
    }
  });
  /**
   * Загрузка собственного фона для вывески
   */
  $(".cll-preview-btns .btn-fon-load").click(function (e) {
    input.trigger("click");
  });
  $(input).change(function () {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#cll-preview--fon-load img").attr("src", e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
      $(".cll-preview--fon").addClass("hidden");
      $("#cll-preview--fon-load").removeClass("hidden");
    }
  });

  $(inputTextPreview).keyup(function () {
    var text = $(this).val();
    $(elTextPreview).html(text);
    let fontSize = $("input[name=text-height]").val();
    updateBorder(fontSize);
  });

  $(".cll-preview-text").draggable();

  /**
   * Выбор шрифта для надписи
   */
  var selectFontFamily = $("select[name=font-family]");
  $(selectFontFamily).change(function (e) {
    var value = $(this).val();
    switch (value) {
      case "arial":
        $(elTextPreview).css("font-family", "Arial");
        $(elTextPreview).css("font-weight", "bold");
        break;
      case "century-gothic-bold":
        $(elTextPreview).css("font-family", "Century Gothic");
        $(elTextPreview).css("font-weight", "bold");
        break;
      case "impact":
        $(elTextPreview).css("font-family", "IMPACT");
        $(elTextPreview).css("font-weight", "normal");
        break;
      case "intro":
        $(elTextPreview).css("font-family", "INTRO");
        $(elTextPreview).css("font-weight", "normal");
        break;
      case "plakat":
        $(elTextPreview).css("font-family", "a_plakatcmpl-extrabold");
        break;
      default:
        $(elTextPreview).css("font-family", "Arial");
        $(elTextPreview).css("font-weight", "bold");
        break;
    }
  });

  /**
   * Выбор цвета - лицо
   */
  $("#ColorTextFace").change(function (e) {
    let colorText = $(this).val();
    setColorText(colorText);
  });

  /**
   * Выбор цвета - борт
   */
  $("#ColorTextBoard").change(function (e) {
    let color = $(this).val();
    setColorBoard(color);
  });

  /**
   * Включение подложки
   */
  $("#RadioSubstrate").change(function (e) {
    if ($(this).prop("checked") == true) {
      $(elSubstrateOptions).removeClass("hidden");
      $(elSubstrate).removeClass("hidden");
      $(".form-group-price-extended").removeClass("hidden");
    } else {
      $(elSubstrateOptions).addClass("hidden");
      $(elSubstrate).addClass("hidden");
      $(".form-group-price-extended").addClass("hidden");
    }
  });

  $("#CheckboxSubstrateAutoSize").change(function (e) {
    let fontSize = $(".text-preview").css("font-size");
    fontSize = fontSize.substring(0, fontSize.length - 2);
    updateBorder(fontSize);
    if ($(this).prop("checked") == true) {
      $("input[name='substrate-size-h']").attr("readonly", "readonly");
      $("input[name='substrate-size-w']").attr("readonly", "readonly");
    } else {
      $("input[name='substrate-size-h']").removeAttr("readonly");
      $("input[name='substrate-size-w']").removeAttr("readonly");
    }
  });

  /********************************/
  /********************************/
  /** Подложка ********************/
  /********************************/

  /**
   * Выбор цвета для подложки
   */
  $("input[name=substrate-color]").change(function (e) {
    let color = $(this).val();
    $("#Substrate").css("background", color);
  });

  /**
   *
   */
  $("input[name=substrate-size-h]").change(function (e) {
    console.log(this);
    if ($("#CheckboxSubstrateAutoSize").prop("checked") == false) {
      let h = $(this).val();
      console.log(h);
      $(elSubstrate).css("height", h);
    }
  });
  $("input[name=substrate-size-w]").change(function (e) {
    if ($("#CheckboxSubstrateAutoSize").prop("checked") == false) {
      let w = $(this).val();
      $(elSubstrate).css("width", w);
    }
  });

  function resetText() {
    $(elTextPreview).removeAttr("style");

    let colorText = $("input[name='text-face']").val();
    setColorText(colorText);
    let colorBoard = $("input[name='text-board']").val();
    let colorSubstrate = $("input[name='substrate-color']").val();

    updateBorder(50);
  }

  /**
   * Обновление всей вывески
   */
  function updateBorder(fontSize) {
    $(elTextPreview).css("font-size", fontSize + "px");
    let h = fontSize;
    let w = roundTo5($(elTextPreview).width());
    $(elSubstrateMathH).html(h);
    $(elSubstrateMathW).html(w);
    $("input[name=text-height]").val(h);
    $("input[name=text-width]").val(w);
    // "#SliderHeight"
    sliderTextHeight.slider("value", fontSize);
    $("#SliderHeightHandle").text(fontSize);

    /**
     * Подложка
     */

    if ($("#CheckboxSubstrateAutoSize").prop("checked") == true) {
      let heightSubstrate = $(elTextPreview).height();
      let widthSubstrate = $(elTextPreview).width();
      $(elSubstrate).css("height", heightSubstrate);
      $(elSubstrate).css("width", widthSubstrate);
    }
    $("input[name=substrate-size-h]").val(roundTo5($(elSubstrate).height()));
    $("input[name=substrate-size-w]").val(roundTo5($(elSubstrate).width()));
  }

  /**
   * Задать цвет текста для вывески
   * @param {*} color
   */
  function setColorText(color) {
    switch ($("select[name=text-type]").val()) {
      case "text-type-1":
        // Световые объемные буквы
        color1Text = color;
        color1TextShadow = color + " 0px 0px 10px";
        $(elTextPreviewType1).css("color", color1Text);
        $(elTextPreviewType1).css("text-shadow", color1TextShadow + ", " + color1Board);
        break;
      case "text-type-2":
        // Объемные буквы со световым торцом и лицом
        color2Text = color;
        $(elTextPreviewType2).css("color", color2Text);

        break;
      case "text-type-3":
        // Объемные буквы с контражурной подсветкой
        color3Text = color;
        $(elTextPreviewType3).css("color", color3Text);

        break;
      case "text-type-4":
        // Объемные буквы без подсветки
        color4Text = color;
        $(elTextPreviewType4).css("color", color4Text);
        break;
      case "text-type-5":
        // Плоские буквы
        $(elTextPreviewType5).css("color", color);
        break;
      case "text-type-6":
        // Плоские буквы с контражурной подсветкой
        $(elTextPreviewType6).css("color", color);
        break;
      default:
        break;
    }
  }

  /**
   * Задать цвет текста для борта или подстветки
   * @param {*} color
   */
  function setColorBoard(color) {
    switch ($("select[name=text-type]").val()) {
      case "text-type-1":
        // Световые объемные буквы
        color1Board =
          color +
          " 0px 1px 0px, " +
          color +
          " 0px 2px 0px, " +
          color +
          " 0px 3px 0px, " +
          color +
          " 0px 4px 0px, " +
          color +
          " 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px, rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, rgb(0, 0, 0) 0px 6px 5px";

        $(elTextPreviewType1).css("text-shadow", color1TextShadow + ", " + color1Board);
        break;
      case "text-type-2":
        // Объемные буквы со световым торцом и лицом
        color2TextShadow = color + " 2px 0px 2px, " + color + " 0px 2px 2px, " + color + " -2px 0px 2px, " + color + " 0px -2px 2px";
        color2Board =
          color +
          " 0px 1px 0px, " +
          color +
          " 0px 2px 0px, " +
          color +
          " 0px 3px 0px, " +
          color +
          " 0px 4px 0px, " +
          color +
          " 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px, rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, 0px 6px 5px rgba(0, 0, 0, 1)";
        $(elTextPreviewType2).css("text-shadow", color2TextShadow + ", " + color2Board);
        break;
      case "text-type-3":
        // Объемные буквы с контражурной подсветкой
        color3Board = color + " 0px 1px 0px";
        color3Board += "," + color + " 0px 2px 0px";
        color3Board += "," + color + " 0px 3px 0px";
        color3Board += "," + color + " 0px 4px 0px";
        color3Board += "," + color + " 0px 5px 0px";
        color3Board += ",rgba(0,0,0,0.1) 0px 6px 1px,rgba(0,0,0,0.1) 0px 0px 5px,rgba(0,0,0,0.3) 0px 1px 3px,rgba(0,0,0,0.2) 0px 3px 5px,rgba(0,0,0,0.25) 0px 5px 10px,rgba(0,0,0,0.2) 0px 10px 10px,rgba(0,0,0,0.15) 0px 20px 20px,10px 0px 15px #fff,0px 10px 15px #fff,-10px 0px 15px #fff,0px -10px 15px #fff,0px 6px 5px #000";
        $(elTextPreviewType3).css("text-shadow", color3Board);
        break;
      case "text-type-4":
        // Объемные буквы без подсветки
        color4Board = color + " 0px 1px 0px";
        color4Board += ", " + color + " 0px 2px 0px";
        color4Board += ", " + color + " 0px 3px 0px";
        color4Board += ", " + color + " 0px 4px 0px";
        color4Board += ", " + color + " 0px 5px 0px";
        color4Board += ", rgba(0, 0, 0, 0.1) 0px 6px 1px, rgba(0, 0, 0, 0.1) 0px 0px 5px, rgba(0, 0, 0, 0.3) 0px 1px 3px, rgba(0, 0, 0, 0.2) 0px 3px 5px, rgba(0, 0, 0, 0.25) 0px 5px 10px,rgba(0, 0, 0, 0.2) 0px 10px 10px, rgba(0, 0, 0, 0.15) 0px 20px 20px, 0px 6px 5px rgba(0, 0, 0, 1)";
        $(elTextPreviewType4).css("text-shadow", color4Board);

        break;
      case "text-type-5":
        // Плоские буквы
        break;
      case "text-type-6":
        // Плоские буквы с контражурной подсветкой
        $(elTextPreviewType6).css("text-shadow", "1px 0px 0px #888, 0px 1px 0px #888, -1px 0px 0px #888, 0px -1px 0px #888, 10px 0px 15px " + color + ", 0px 10px 15px " + color + ", -10px 0px 15px " + color + ", 0px -10px 15px " + color + ", 0px 6px 5px rgba(0, 0, 0, 1)");
        break;

      default:
        break;
    }
  }

  /**
   * Подсказка стоимости
   */
  function changeQuestionData() {
    let text = "";
    switch ($("select[name=text-type]").val()) {
      case "text-type-1":
        // Световые объемные буквы
        text = "100 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      case "text-type-2":
        // Объемные буквы со световым торцом и лицом
        text = "120 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      case "text-type-3":
        // Объемные буквы с контражурной подсветкой
        text = "130 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      case "text-type-4":
        // Объемные буквы без подсветки
        text = "65 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      case "text-type-5":
        // Плоские буквы
        text = "50 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      case "text-type-6":
        // Плоские буквы с контражурной подсветкой
        text = "100 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2";
        break;
      default:
        break;
    }
    $(".from-cli-question-data").html(text);
  }

  /**
   * Расчет стоимости
   */
  function calcPrice() {
    let priceBoard = 0;
    let height = $("input[name=text-height]").val();
    let text = $("input[name=text-preview]").val();
    let count = text.length;
    // Надпись
    switch ($("select[name=text-type]").val()) {
      case "text-type-1":
        // Световые объемные буквы
        // 90 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 100 * height * count;
        break;
      case "text-type-2":
        // Объемные буквы со световым торцом и лицом
        // 120 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 120 * height * count;
        break;
      case "text-type-3":
        // Объемные буквы с контражурной подсветкой
        // 130 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 130 * height * count;
        break;
      case "text-type-4":
        // Объемные буквы без подсветки
        // 65 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 65 * height * count;

        break;
      case "text-type-5":
        // Плоские буквы
        // 50 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 50 * height * count;
        break;
      case "text-type-6":
        // Плоские буквы с контражурной подсветкой
        // 100 руб. за 1 см, при высоте менее 20 см вводится коэффициент 1.2
        priceBoard = 100 * height * count;
        break;
      default:
        break;
    }
    // При высоте менее 20 см вводится коэффициент 1.2
    if (height < 20) {
      priceBoard = priceBoard * 1.2;
    }
    $("input[name=price-board]").val(priceBoard);

    // Подложка
    // Расчет подложки : по площади  1 м2 - 5000 руб. Минимальный заказ 1 м2
    let priceSubstrate = 0;
    if ($("#RadioSubstrate").prop("checked") == true) {
      let h = $("#Substrate").height();
      let w = $("#Substrate").width();
      let s = (h / 100) * (w / 100);
      priceSubstrate = s * 5000;
      priceSubstrate = Math.round(priceSubstrate);
    }
    $("input[name=price-substrate]").val(priceSubstrate);

    // Итоговая цена
    let price = priceBoard + priceSubstrate;
    $("input[name=price]").val(price);
  }

  $("form.from-cli label").click(function (e) {
    calcPrice();
    changeQuestionData();
  });
  $("form.from-cli input").click(function (e) {
    calcPrice();
    changeQuestionData();
  });
  $("form.from-cli select").click(function (e) {
    calcPrice();
    changeQuestionData();
  });
  $("form.from-cli input").change(function (e) {
    calcPrice();
    changeQuestionData();
  });
  $("form.from-cli input").keyup(function (e) {
    calcPrice();
    changeQuestionData();
  });

  updateBorder(50);
  calcPrice();

  $(".message--btn").click(function (e) {
    $(".cli-message").removeClass("active");
  });
});

function roundTo5(num) {
  return Math.round(num / 2) * 2;
}
